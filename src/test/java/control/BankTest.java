package control;


import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


class BankTest {
    static Bank bank;
    static Person person;
    static SpendingAccount spendingAccount;
    static SavingAccount savingAccount;
    @BeforeAll
    public static void setup(){
        bank = new Bank();
        person = new Person("Mihai", "str. Ciobanului", "gigi@gigi.com", 19);
        spendingAccount = new SpendingAccount(1500);
        savingAccount = new SavingAccount(1500.0, 15, 36);
        bank.addPerson(person);
        bank.addAccount(person, spendingAccount);
        bank.addAccount(person, savingAccount);
    }

    @Test
    public void test(){
        ArrayList<Account> accounts = bank.listAccounts();
        assertEquals(accounts.size(), 2);
        try {
            spendingAccount.withdraw(500);
            assertEquals(spendingAccount.getAvailableMoney(), 1000.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(savingAccount.getAvailableMoney(), 1522.5);
    }
}