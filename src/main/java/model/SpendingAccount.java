package model;

import java.io.Serializable;

public class SpendingAccount extends Account {
    //allows several deposits and withdrawals
    private static int id = 0;

    public SpendingAccount(double sum, int holderId, int id){
        super(id, holderId, sum, "Spending");
    }

    public SpendingAccount(double sum){
        super((++id), "Spending", sum);
    }

    public void deposit(double sum){
        notify("deposit");
        setAvailableMoney(getAvailableMoney() + sum);
    }

    public void withdraw(double sum) throws Exception {
        if(sum > getAvailableMoney()){
            throw new Exception("insufficient funds");
        }else {
            notify("withdraw");
            setAvailableMoney(getAvailableMoney() - sum);
        }
    }
    public String toString(){
        return super.toString();
    }
}
