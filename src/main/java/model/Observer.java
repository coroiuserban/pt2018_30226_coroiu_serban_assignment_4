package model;

public interface Observer {
    public void notify(String source);
}
