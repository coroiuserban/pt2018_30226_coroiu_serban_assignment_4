package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Observer,Serializable {
    private int id;
    private int accountHolder;
    private String accountType;
    private double availableMoney;
    // pune suma initiala si la toString fa afisare pe amandoua + dobanda si perioada
    public Account(int id, String accountType, double sum){
        this.id = id;
        this.accountType = accountType;
        this.availableMoney = sum;
    }

    public Account(int id, int accountHolder, double sum, String accountType){
        this.id = id;
        this.accountHolder = accountHolder;
        this.availableMoney = sum;
        this.accountType = accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setAvailableMoney(double availableMoney) {
        this.availableMoney = availableMoney;
    }

    public int getId() {
        return id;
    }

    public double getAvailableMoney() {
        return availableMoney;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountHolder(int id){
        this.accountHolder = id;
    }

    public int getAccountHolder(){
        return accountHolder;
    }

    public String toString(){
        return "Holder: "+accountHolder+". Accout with id: "+id+" is "+accountType+" account.\nAvailable funds: "+availableMoney+" ";
    }

    public void notify(String source) {
        System.out.println("A "+source+" has been made on account "+id+" of client "+accountHolder+".");
    }
}
