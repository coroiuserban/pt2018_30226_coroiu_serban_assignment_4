package model;

import java.io.Serializable;

public class Person implements Serializable {
    private int id;
    private String name;
    private String adress;
    private String email;

    public Person(){
        name = "";
        adress = "";
        id = 0;
        email = "";
    }

    public Person(String name, String adress, String email, int id){
        this.name = name;
        this.adress = adress;
        this.email = email;
        this.id = id;
    }

    public String getName(){  return name;    }

    public String getAdress(){return adress;}

    public String getEmail(){return email;}

    public int getId(){return id;}

    public void setName(String name){ this.name = name; }

    public void setAdress(String adress){ this.adress = adress; }

    public void setEmail(String email){ this.email = email; }

    public void setId(int id){ this.id = id; }

    public String toString(){
        return "Person: "+name+" has id: "+id;
    }
}
