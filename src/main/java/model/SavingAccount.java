package model;

import java.io.Serializable;
import java.text.NumberFormat;

public class SavingAccount extends Account {
    private static int id = 0;
    private int rate;   //only percentage
    private int period; //months
    private double initialMoney;

    public SavingAccount(double sum, int rate, int period){
        super((++id), "Saving", sum + sum*((double)rate/100.0)*(double)period/360.0);
        this.initialMoney = sum;
        this.rate = rate;
        this.period = period;

    }

    public int getRate() {
        return rate;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setAvailableMoney(double sum){
        double s = sum + sum*((double)rate/100.0)*(double)period/360.0;
        initialMoney = sum;
        super.setAvailableMoney(s);
    }

    public double withdrawDeposit(){
        double money = getAvailableMoney();
        super.setAvailableMoney(0.0);
        initialMoney = 0.0;
        notify("withdraw");
        return money;
    }

    public String toString(){
        return super.toString() +"Initial funds: " +initialMoney+". Deposit for " +period+" months with "+rate+"% interest\n";
    }
}
