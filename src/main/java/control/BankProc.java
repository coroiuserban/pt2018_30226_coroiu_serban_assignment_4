package control;

import model.Account;
import model.Person;

import java.util.ArrayList;
import java.util.List;

public interface BankProc {
        void addPerson(Person person);
        void removePerson(Person person);
        void updatePerson(Person person);
        void addAccount(Person person, Account account);
        void removeAccount(Person person, int accountId);
        void updateAccount(Person person, int accountId, Account newAccount);
        ArrayList<Person> listPersons();
        ArrayList<Account> listAccounts();
}
