package control;

import model.Account;
import model.Person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class Bank implements BankProc, Serializable{
    private Hashtable<Person, ArrayList<Account>> bank = new Hashtable<Person, ArrayList<Account>>();

    public void addPerson(Person person) {
        assert (person != null) : "null person";
        ArrayList<Account> accounts = new ArrayList<Account>();
        bank.put(person, accounts);
    }

    public void removePerson(Person person) {
        assert (person != null) : "null person";
        assert findPerson(person) : "Person was not found(Remove)";
        bank.remove(person);
    }

    public void updatePerson(Person newPerson) {
        assert (newPerson != null) : "null person";
        assert findPerson(newPerson) : "Person was not found(Update)";
        Person oldPerson = getHolder(newPerson.getId());
        ArrayList<Account> accounts = bank.remove(getHolder(newPerson.getId()));
        if(!newPerson.getName().equals("name"))
            oldPerson.setName(newPerson.getName());
        if(!newPerson.getAdress().equals("adress"))
            oldPerson.setAdress(newPerson.getAdress());
        if(!newPerson.getEmail().equals("email"))
            oldPerson.setEmail(newPerson.getEmail());
        bank.put(oldPerson, accounts);
    }

    public void addAccount(Person person, Account account) {
        check(person, account);
        account.setAccountHolder(person.getId());
        bank.get(person).add(account);
    }

    public void removeAccount(Person person, int accountId) {
        assert findAccount(person, accountId) : "Account was not found";
        ArrayList<Account> accounts = bank.get(person);
        for(Account acc : accounts){
            if(acc.getId() == accountId)
                accounts.remove(acc);
        }
        bank.put(person, accounts);
    }

    public void updateAccount(Person person, int accountId, Account newAccount) {
        assert findAccount(person, accountId): "Account was not found";
        assert (!newAccount.getAccountType().equals("Saving")) : "SavingAccount could not be modified\n";
        ArrayList<Account> accounts = bank.get(person);
        for (Account acc : accounts)
            if (acc.getId() == accountId) {
                assert (!acc.getAccountType().equals("Saving")) : "SavingAccount could not be modified\n";
                //SpendingAccount : sum
                if (newAccount.getAvailableMoney() != acc.getAvailableMoney()) {
                    acc.setAvailableMoney(newAccount.getAvailableMoney());
                }
                if(newAccount.getAccountHolder() != acc.getAccountHolder()){
                    acc.setAccountHolder(newAccount.getAccountHolder());
                }
            }
    }

    public ArrayList<Person> listPersons() {
        ArrayList<Person> persons = new ArrayList<Person>();
        Set<Person> set = bank.keySet();
        for(Person p: set){
            persons.add(p);
        }
        return persons;
    }

    public ArrayList<Account> listAccounts(){
        ArrayList<Account> accounts = new ArrayList<Account>();
        for(Person p : bank.keySet())
            for(Account account : bank.get(p)){
                accounts.add(account);
            }
        return accounts;
    }

    public Account getAccount(int personId, int accountId){
        ArrayList<Account> accounts = bank.get(getHolder(personId));
        for(Account account: accounts)
            if(account.getId() == accountId)
                return account;
        return null;
    }

    public Account getAccount(int personId, int accountId, String type){
        ArrayList<Account> accounts = bank.get(getHolder(personId));
        for(Account account: accounts)
            if(account.getId() == accountId && account.getAccountType().equals(type))
                return account;
        assert false : "Account was not found(getAccount).";
        return null;
    }

    public Person getHolder(int personId){
        Set<Person> people = bank.keySet();
        for(Person p : people)
            if(p.getId() == personId)
                return p;
        return null;
    }

    private boolean findPerson(Person person){
        Set<Person> people = bank.keySet();
        boolean found = false;
        for(Person p : people){
            if(p.getId() == person.getId()) {
                found = true;
            }
        }
        return found;
    }

    private boolean findAccount(Person person, int accountId){
        boolean found = false;
        for(Account acc:bank.get(person))
            if(acc.getId() == accountId) {
                found = true;
                break;
            }
        return found;
    }

    private void check(Person person, Account account){
        assert person != null : "null person";
        assert account != null : "null account";
        assert findPerson(person) : "person was not found";
    }

    public Hashtable<Person, ArrayList<Account>> getData(){
        return bank;
    }

}
