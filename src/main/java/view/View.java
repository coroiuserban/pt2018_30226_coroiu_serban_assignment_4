package view;

import control.Bank;

import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class View{
    private JFrame frame;
    private JTable personTable;
    private JTable accountTable;
    private DefaultTableModel personModel;
    private static DefaultTableModel accountModel;
    private Object[][] data;
    private String[] columnNames;
    private static Bank bank = new Bank();

    public View(){
        try {
            FileInputStream fileIn = new FileInputStream("bank.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            bank = (Bank)in.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        createWindow();
    }

    private void createWindow(){
        frame = new JFrame("User Interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setBounds(330, 200, 1000, 500);

        //table create                ----------------------------------------------------------------------------------------
        columnNames = new String[Person.class.getDeclaredFields().length];
        headerInit(Person.class.getDeclaredFields());
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(50,220,380,200);
        personTable = new JTable();
        personModel = new DefaultTableModel(data, columnNames);
        personTable.setModel(personModel);
        scrollPane.setViewportView(personTable);
        frame.add(scrollPane);

        columnNames = new String[Account.class.getDeclaredFields().length];
        headerInit(Account.class.getDeclaredFields());
        scrollPane = new JScrollPane();
        scrollPane.setBounds(550,220,380,200);
        accountTable = new JTable();
        accountModel = new DefaultTableModel(data, columnNames);
        accountTable.setModel(accountModel);
        scrollPane.setViewportView(accountTable);
        frame.add(scrollPane);
        refreshAccounts();
        refreshPeople();

        //insert          ----------------------------------------------------------------------------------------
        JLabel lInsertPerson = new JLabel("Insert client:");
        lInsertPerson.setBounds(10, 20, 70, 20);
        frame.add(lInsertPerson);
        final JTextField tfIdInsertPerson = new JTextField();
        tfIdInsertPerson.setText("ID");
        tfIdInsertPerson.setBounds(90, 20, 40, 20);
        frame.add(tfIdInsertPerson);
        final JTextField tfNameInsertPerson = new JTextField("name");
        tfNameInsertPerson.setBounds(140, 20, 100, 20);
        frame.add(tfNameInsertPerson);
        final JTextField tfAdressInsertPerson = new JTextField("adress");
        tfAdressInsertPerson.setBounds(250, 20, 120, 20);
        frame.add(tfAdressInsertPerson);
        final JTextField tfEmailInsertPerson = new JTextField("email");
        tfEmailInsertPerson.setBounds(380, 20, 110, 20);
        frame.add(tfEmailInsertPerson);
        JButton btnInsertPerson = new JButton("Insert");
        btnInsertPerson.setBounds(40, 140, 90, 20);
        btnInsertPerson.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(tfIdInsertPerson.getText());
                String name = tfNameInsertPerson.getText();
                String adress = tfAdressInsertPerson.getText();
                String email = tfEmailInsertPerson.getText();
                Person person = new Person(name, adress, email, id);
                bank.addPerson(person);
                refreshPeople();
            }
        });
        frame.add(btnInsertPerson);
        // update person              ----------------------------------------------------------------------------------------
        JLabel lUpdatePerson = new JLabel("Update client:");
        lUpdatePerson.setBounds(10, 60, 80, 20);
        frame.add(lUpdatePerson);
        final JTextField tfIdUpdatePerson = new JTextField("ID");
        tfIdUpdatePerson.setBounds(90, 60, 40, 20);
        frame.add(tfIdUpdatePerson);
        final JTextField tfNameUpdatePerson = new JTextField("name");
        tfNameUpdatePerson.setBounds(140, 60, 100, 20);
        frame.add(tfNameUpdatePerson);
        final JTextField tfAdressUpdatePerson = new JTextField("adress");
        tfAdressUpdatePerson.setBounds(250, 60, 120, 20);
        frame.add(tfAdressUpdatePerson);
        final JTextField tfEmailUpdatePerson = new JTextField("email");
        tfEmailUpdatePerson.setBounds(380, 60, 110, 20);
        frame.add(tfEmailUpdatePerson);
        JButton btnUpdatePerson = new JButton("Update");
        btnUpdatePerson.setBounds(195, 140, 90, 20);
        btnUpdatePerson.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(tfIdUpdatePerson.getText());
                String name = tfNameUpdatePerson.getText();
                String adress = tfAdressUpdatePerson.getText();
                String email = tfEmailUpdatePerson.getText();
                Person person = new Person(name, adress, email, id);
                System.out.println(person);
                bank.updatePerson(person);
                refreshPeople();
            }
        });
        frame.add(btnUpdatePerson);

        //delete                      ----------------------------------------------------------------------------------------
        JLabel lDeletePerson = new JLabel("Delete client:");
        lDeletePerson.setBounds(10, 100, 80, 20);
        frame.add(lDeletePerson);
        final JTextField tfIdDeletePerson = new JTextField("ID");
        tfIdDeletePerson.setBounds(90, 100, 80, 20);
        frame.add(tfIdDeletePerson);
        JButton btnDeletePerson = new JButton("Delete");
        btnDeletePerson.setBounds(350, 140, 90, 20);
        btnDeletePerson.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int personId = Integer.parseInt(tfIdDeletePerson.getText());
                Person person = bank.getHolder(personId);
                bank.removePerson(person);
                refreshPeople();
            }
        });
        frame.add(btnDeletePerson);

        //Account                                                -------------------create account--------------
        JLabel lInsertAccount = new JLabel("Create account for person id:");
        lInsertAccount.setBounds(600, 20, 180, 20);
        frame.add(lInsertAccount);
        final JTextField tfIdCreateAccount = new JTextField("ID");
        tfIdCreateAccount.setBounds(790, 20, 40, 20);
        frame.add(tfIdCreateAccount);
        JButton btnCreateSpending = new JButton("Spending account");
        btnCreateSpending.setBounds(600,50, 150, 20);
        frame.add(btnCreateSpending);
        btnCreateSpending.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int personId = Integer.parseInt(tfIdCreateAccount.getText());
                Person person = bank.getHolder(personId);
                JTextField sum = new JTextField();
                Object[] message = {"Sum:", sum,};
                JOptionPane.showConfirmDialog(null, message, "SpendingAccount", JOptionPane.OK_CANCEL_OPTION);
                SpendingAccount account = new SpendingAccount(Double.parseDouble(sum.getText()));
                //System.out.println(account);
                bank.addAccount(person, account);
                refreshAccounts();
            }
        });
        JButton btnCreateSaving = new JButton("Saving account");
        btnCreateSaving.setBounds(800,50, 150, 20);
        btnCreateSaving.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int personId = Integer.parseInt(tfIdCreateAccount.getText());
                Person person = bank.getHolder(personId);
                JTextField sum = new JTextField();
                JTextField rate = new JTextField();
                JTextField period = new JTextField();
                Object[] message = {"Sum:", sum, "Rate:", rate, "Period:", period};
                JOptionPane.showConfirmDialog(null, message, "SpendingAccount", JOptionPane.OK_CANCEL_OPTION);
                SavingAccount account = new SavingAccount(Double.parseDouble(sum.getText()), Integer.parseInt(rate.getText()), Integer.parseInt(period.getText()));
                bank.addAccount(person, account);
                refreshAccounts();
            }
        });
        frame.add(btnCreateSaving);
        //update                                                              ---------------update account----------------
        JLabel lUpdateAccount = new JLabel("Update account for person id:");
        lUpdateAccount.setBounds(600, 80, 180, 20);
        frame.add(lUpdateAccount);
        final JTextField tfIdUpdateAccount = new JTextField("PID");
        tfIdUpdateAccount.setBounds(790, 80, 40, 20);
        frame.add(tfIdUpdateAccount);
        final JTextField tfIdAccount = new JTextField("AID");
        tfIdAccount.setBounds(840, 80, 40, 20);
        frame.add(tfIdAccount);
        JButton btnUpdateSpending = new JButton("Spending account");
        btnUpdateSpending.setBounds(600,110, 150, 20);
        btnUpdateSpending.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int accountId = Integer.parseInt(tfIdAccount.getText());
                int personId = Integer.parseInt(tfIdUpdateAccount.getText());
                JTextField sum = new JTextField();
                JTextField holder = new JTextField();
                Object[] message = {"Holder:", holder, "Sum:", sum};
                JOptionPane.showConfirmDialog(null, message, "Update", JOptionPane.OK_CANCEL_OPTION);
                SpendingAccount account = new SpendingAccount(Double.parseDouble(sum.getText()), Integer.parseInt(holder.getText()), accountId);
                bank.updateAccount(bank.getHolder(personId), accountId, account);
                refreshAccounts();
            }
        });
        frame.add(btnUpdateSpending);
        //operations allowed on spending  ----------------------------------------------------------------------------------------
        JButton btnDepositSpending = new JButton("Deposit");
        btnDepositSpending.setBounds(600,160, 150, 20);
        btnDepositSpending.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshAccounts();
                JTextField sum = new JTextField();
                JTextField accountId = new JTextField();
                JTextField holderId = new JTextField();
                Object[] message = {"Holder:", holderId,"Account:", accountId, "Sum:", sum};
                JOptionPane.showConfirmDialog(null, message, "Update", JOptionPane.OK_CANCEL_OPTION);
                Account account = bank.getAccount(Integer.parseInt(holderId.getText()), Integer.parseInt(accountId.getText()));
                ((SpendingAccount) account).deposit(Double.parseDouble(sum.getText()));
                refreshAccounts();
            }
        });
        frame.add(btnDepositSpending);
        JButton btnWithdrawSpending = new JButton("Withdraw");
        btnWithdrawSpending.setBounds(800,160, 150, 20);
        btnWithdrawSpending.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshAccounts();
                JTextField sum = new JTextField();
                JTextField accountId = new JTextField();
                JTextField holderId = new JTextField();
                Object[] message = {"Holder:", holderId,"Account:", accountId, "Sum:", sum};
                JOptionPane.showConfirmDialog(null, message, "Update", JOptionPane.OK_CANCEL_OPTION);
                Account account = bank.getAccount(Integer.parseInt(holderId.getText()), Integer.parseInt(accountId.getText()), "Spending");
                try {
                    ((SpendingAccount) account).withdraw(Double.parseDouble(sum.getText()));
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null,e1.getMessage()+".Available: "+account.getAvailableMoney());
                }
                refreshAccounts();
            }
        });
        frame.add(btnWithdrawSpending);
        //  --------------------------------------------------------------operations allowed on saving account-------------------------------------------------------------------
        JButton btnWithdrawSaving = new JButton("Close Saving Account");
        btnWithdrawSaving.setBounds(680, 190, 200, 20);
        btnWithdrawSaving.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshAccounts();
                JTextField accountId = new JTextField();
                JTextField holderId = new JTextField();
                Object[] message = {"Holder:", holderId,"Account:", accountId};
                JOptionPane.showConfirmDialog(null, message, "Update", JOptionPane.OK_CANCEL_OPTION);
                Person person = bank.getHolder(Integer.parseInt(holderId.getText()));
                Account account = (SavingAccount)bank.getAccount(Integer.parseInt(holderId.getText()), Integer.parseInt(accountId.getText()), "Saving");
                //System.out.println(account);
                account.setAvailableMoney(((SavingAccount) account).withdrawDeposit());
                JOptionPane.showMessageDialog(null, account);
                bank.removeAccount(person, Integer.parseInt(accountId.getText()));
                refreshAccounts();
            }
        });
        frame.add(btnWithdrawSaving);
        //save&load bank data   ----------------------------------------------------------------------------------------
        JButton btnSaveData = new JButton("Save");
        btnSaveData.setBounds(450, 300, 80,20);
        btnSaveData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    FileOutputStream fileOut = new FileOutputStream("bank.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(bank);
                    out.close();
                    fileOut.close();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        frame.add(btnSaveData);

        //-----------------------------------------------------------------click listener -----------------------------------
        TableListener tableListener = new TableListener(accountTable, bank);
        accountTable.addMouseListener(tableListener);
        frame.setVisible(true);
    }

    private void headerInit(Field[] declaredFields){
        int i = 0;
        //System.out.println(declaredFields.length);
        for(Field field: declaredFields) {
            columnNames[i++] = field.getName();
        }
    }

    private void refreshPeople(){
        personModel.setRowCount(0);
        ArrayList<Person> people = bank.listPersons();
        Object[] details = new Object[Person.class.getDeclaredFields().length];
        for(Person p : people){
            int i = 0;
            for (Field field : p.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    details[i++] = field.get(p);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            personModel.addRow(details);
        }
    }

    static void refreshAccounts(){
        accountModel.setRowCount(0);
        ArrayList<Account> accounts = bank.listAccounts();
        Object[] details = new Object[Account.class.getDeclaredFields().length];
        int i=0;
        for(Account account: accounts){
            details[0] = account.getId();
            details[1] = account.getAccountHolder();
            details[2] = account.getAccountType();
            details[3] = account.getAvailableMoney();
            accountModel.addRow(details);
        }
    }
}
