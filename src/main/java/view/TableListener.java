package view;

import control.Bank;
import model.Account;
import model.Person;
import model.SavingAccount;
import sun.security.jca.GetInstance;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;
import java.util.List;

public class TableListener implements MouseListener {
    JTable table;
    Bank bank;
    public TableListener(JTable table, Bank bank) {
        this.table = table;
        this.bank = bank;
    }

    public void mouseClicked(MouseEvent e) {
        Point point = e.getPoint();
        int column = table.columnAtPoint(point);
        int row = table.rowAtPoint(point);
        if(table.getValueAt(row, column).equals("Saving")){
            int personId = (Integer)table.getValueAt(row, column-1);
            int accountId = (Integer)table.getValueAt(row, column-2);
            SavingAccount account = (SavingAccount)bank.getAccount(personId, accountId, "Saving");
            JOptionPane.showMessageDialog(null, account);

        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }
}
